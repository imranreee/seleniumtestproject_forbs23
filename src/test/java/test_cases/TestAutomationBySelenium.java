package test_cases;

import org.openqa.selenium.*;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import java.io.IOException;
import java.util.Random;

public class TestAutomationBySelenium {
    WebDriver driver;
    String chromeDriverPath = System.getProperty("user.dir") +"\\src\\test\\java\\utils\\driver\\chromedriver.exe";
    String baseURL = "https://www.phptravels.net/";

    @BeforeTest
    public void browserUpAndRun(){
        System.setProperty("webdriver.chrome.driver", chromeDriverPath);
        driver = new ChromeDriver();
        driver.manage().window().maximize();
        driver.get(baseURL);
        System.out.println("1. "+baseURL+" up and run");
    }

    @Test(priority = 1)
    public void tourBooking() throws Exception {
        By tourOption = By.xpath("//a[contains(text(),'Tours')]");
        By searchField2 = By.xpath("/html[1]/body[1]/div[2]/div[1]/div[1]/div[3]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[5]/div[1]/div[1]/form[1]/div[1]/div[1]/div[1]/div[1]/div[2]/div[1]/a[1]");
        By tourType = By.xpath("//body/div[2]/div[1]/div[1]/div[3]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[5]/div[1]/div[1]/form[1]/div[1]/div[1]/div[2]/div[1]/div[1]/div[1]/div[1]/div[2]/div[1]/a[1]/span[1]");
        By date = By.xpath("//body/div[2]/div[1]/div[1]/div[3]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[5]/div[1]/div[1]/form[1]/div[1]/div[1]/div[3]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[2]/input[1]");
        By yachtType = By.xpath("//li[contains(text(),'Yacht')]");
        By increaseBtn = By.xpath("//body/div[2]/div[1]/div[1]/div[3]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[5]/div[1]/div[1]/form[1]/div[1]/div[1]/div[3]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[2]/div[1]/div[2]/div[1]/span[1]/button[1]");
        By searchBtn = By.xpath("//body/div[2]/div[1]/div[1]/div[3]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[5]/div[1]/div[1]/form[1]/div[1]/div[1]/div[4]/button[1]");
        By bookNowBtn = By.xpath("//body/div[2]/div[1]/div[1]/div[1]/div[4]/div[1]/div[1]/aside[1]/div[1]/form[1]/div[1]/form[1]/button[1]");

        By firstNameField2 = By.xpath("/html[1]/body[1]/div[2]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[2]/div[4]/div[1]/div[1]/div[1]/div[1]/form[1]/div[1]/div[1]/div[1]/label[1]/input[1]");
        By firstNameField3 = By.xpath("/html[1]/body[1]/div[2]/div[1]/section[1]/div[1]/div[1]/div[2]/div[1]/form[1]/div[3]/div[1]/div[1]/label[1]/input[1]");
        By firstNameField = By.xpath("//span[contains(text(),'First Name')]");
        By lastNameField = By.xpath("//span[contains(text(),'Last Name')]");
        By lastNameField2 = By.xpath("/html[1]/body[1]/div[2]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[2]/div[4]/div[1]/div[1]/div[1]/div[1]/form[1]/div[1]/div[2]/label[1]/input[1]");
        By lastNameField3 = By.xpath("/html[1]/body[1]/div[2]/div[1]/section[1]/div[1]/div[1]/div[2]/div[1]/form[1]/div[3]/div[2]/div[1]/label[1]/input[1]");
        By emailField = By.xpath("//span[contains(text(),'Email')]");
        By emailField2 = By.xpath("/html[1]/body[1]/div[2]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[2]/div[4]/div[1]/div[1]/div[1]/div[1]/form[1]/div[2]/div[1]/label[1]/input[1]");
        By emailField3 = By.xpath("/html[1]/body[1]/div[2]/div[1]/section[1]/div[1]/div[1]/div[2]/div[1]/form[1]/div[5]/label[1]/input[1]");
        By confirmField = By.xpath("//span[contains(text(),'Confirm')]");
        By confirmField2 = By.xpath("/html[1]/body[1]/div[2]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[2]/div[4]/div[1]/div[1]/div[1]/div[1]/form[1]/div[2]/div[2]/label[1]/input[1]");
        By contactNumberField = By.xpath("//span[contains(text(),'Contact Number')]");
        By contactNumberField2 = By.xpath("/html[1]/body[1]/div[2]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[2]/div[4]/div[1]/div[1]/div[1]/div[1]/form[1]/div[3]/div[1]/label[1]/input[1]");
        By mobileNumberField = By.xpath("//span[contains(text(),'Mobile Number')]");
        By mobileNumberField2 = By.xpath("/html[1]/body[1]/div[2]/div[1]/section[1]/div[1]/div[1]/div[2]/div[1]/form[1]/div[4]/label[1]/input[1]");
        By addressField = By.xpath("//span[contains(text(),'Address')]");
        By addressField2 = By.xpath("/html[1]/body[1]/div[2]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[2]/div[4]/div[1]/div[1]/div[1]/div[1]/form[1]/div[4]/div[1]/label[1]/input[1]");
        By selectCountry = By.xpath("//span[contains(text(),'Select Country')]");
        By passwordField = By.xpath("//span[contains(text(),'Password')]");
        By passwordField2 = By.xpath("/html[1]/body[1]/div[2]/div[1]/section[1]/div[1]/div[1]/div[2]/div[1]/form[1]/div[6]/label[1]/input[1]");
        By confirmPasswordField = By.xpath("//span[contains(text(),'Confirm Password')]");
        By confirmPasswordField2 = By.xpath("/html[1]/body[1]/div[2]/div[1]/section[1]/div[1]/div[1]/div[2]/div[1]/form[1]/div[7]/label[1]/input[1]");

        By searchFieldCountry = By.xpath("//body/div[2]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[2]/div[4]/div[1]/div[1]/div[1]/div[1]/form[1]/div[5]/div[1]/div[2]/div[1]/div[1]/input[1]");
        By confirmBookingBtn = By.xpath("//body/div[2]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[2]/form[1]/div[9]/button[1]");
        By payOnArrival = By.xpath("/html[1]/body[1]/div[2]/div[1]/div[1]/div[1]/div[1]/div[2]/center[1]/button[1]");
        By myAccountBtn = By.xpath("/html/body/div[2]/header/div[1]/div/div/div[2]/div/ul/li[2]/div/a");
        By signUp = By.linkText("Sign Up");
        By signUpBtn = By.xpath("//body/div[2]/div[1]/section[1]/div[1]/div[1]/div[2]/div[1]/form[1]/div[8]/button[1]");
        By profileAvatar = By.xpath("//body/div[2]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/img[1]");
        By newsletterBtn = By.xpath("//a[contains(text(),'Newsletter')]");
        By enableSubscribeToggleBtn = By.xpath("//body/div[2]/div[1]/div[2]/div[2]/div[1]/div[1]/div[2]/div[1]/div[4]/div[1]/div[1]/label[1]/span[1]");

        System.out.println("== Tour Booking Test Began ==");

        waitForVisibilityOf(tourOption);
        driver.findElement(tourOption).click();
        Thread.sleep(500);
        System.out.println("2. Tour selected from search bar");

        String destination = "Legoland Malaysia Day Pass";
        driver.findElement(searchField2).click();
        driver.findElement(searchField2).sendKeys(destination);
        Thread.sleep(1000);
        driver.findElement(searchField2).sendKeys(Keys.ENTER);
        Thread.sleep(500);
        System.out.println("3. Legoland Malaysia Day Pass as destination selected");

        /*WebElement tourType = driver.findElement(By.id("tourtype"));
        Select tourTypeDropDownList = new Select(tourType);
        tourTypeDropDownList.selectByValue("271");*/

        driver.findElement(tourType).click();
        WebElement element1 = driver.findElement(yachtType);
        ((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", element1);
        Thread.sleep(500);
        driver.findElement(yachtType).click();
        System.out.println("4. Tour type Yacht selected");

        driver.findElement(date).click();
        driver.findElement(date).clear();
        driver.findElement(date).sendKeys("09/07/2021");
        Thread.sleep(500);
        System.out.println("5. Date selected as 09/07/2021");

        waitForVisibilityOf(increaseBtn);
        driver.findElement(increaseBtn).click();
        waitForClickAbilityOf(searchBtn);
        driver.findElement(searchBtn).click();
        System.out.println("6. Increase adult count 2 and clicked on search button");

        driver.navigate().refresh();
        waitForClickAbilityOf(bookNowBtn);
        WebElement element = driver.findElement(bookNowBtn);
        ((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", element);
        Thread.sleep(500);
        driver.findElement(bookNowBtn).click();
        Thread.sleep(500);
        System.out.println("7. Clicked on the Book now button");

        waitForClickAbilityOf(firstNameField);
        String fName = "Al";
        driver.findElement(firstNameField).click();
        driver.findElement(firstNameField2).sendKeys(fName);

        String lName = "Imran";
        waitForClickAbilityOf(lastNameField);
        driver.findElement(lastNameField).click();
        driver.findElement(lastNameField2).sendKeys(lName);

        String email = "imran@gmail.com";
        waitForClickAbilityOf(emailField);
        driver.findElement(emailField).click();
        driver.findElement(emailField2).sendKeys(email);

        waitForClickAbilityOf(confirmField);
        driver.findElement(confirmField).click();
        driver.findElement(confirmField2).sendKeys(email);

        WebElement element2 = driver.findElement(contactNumberField);
        ((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", element2);
        Thread.sleep(500);
        String contactNo = "+8801911000000";
        waitForClickAbilityOf(contactNumberField);
        driver.findElement(contactNumberField).click();
        driver.findElement(contactNumberField2).sendKeys(contactNo);

        String address = "Dhaka, Bangladesh";
        waitForClickAbilityOf(addressField);
        driver.findElement(addressField).click();
        driver.findElement(addressField2).sendKeys(address);

        waitForClickAbilityOf(selectCountry);
        driver.findElement(selectCountry).click();
        waitForClickAbilityOf(searchFieldCountry);
        String countryName = "Bangladesh";
        driver.findElement(searchFieldCountry).sendKeys(countryName);
        Thread.sleep(1000);
        driver.findElement(searchFieldCountry).sendKeys(Keys.ENTER);

        WebElement element3 = driver.findElement(confirmBookingBtn);
        ((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", element3);
        waitForClickAbilityOf(confirmBookingBtn);
        driver.findElement(confirmBookingBtn).click();

        waitForClickAbilityOf(payOnArrival);
        Thread.sleep(1000);
        driver.findElement(payOnArrival).click();
        Thread.sleep(5000);
        driver.switchTo().alert().accept();
        System.out.println("8. Booking completed as a guest and completed payment with Pay on Arrival.");

        waitForClickAbilityOf(myAccountBtn);
        Thread.sleep(1000);
        driver.findElement(myAccountBtn).click();

        waitForClickAbilityOf(signUp);
        Thread.sleep(1000);
        driver.findElement(signUp).click();

        waitForClickAbilityOf(firstNameField);
        driver.findElement(firstNameField).click();
        driver.findElement(firstNameField3).sendKeys(fName);

        waitForClickAbilityOf(lastNameField);
        driver.findElement(lastNameField).click();
        driver.findElement(lastNameField3).sendKeys(lName);

        waitForClickAbilityOf(mobileNumberField);
        driver.findElement(mobileNumberField).click();
        driver.findElement(mobileNumberField2).sendKeys(contactNo);

        Random randomGenerator = new Random();
        int randomInt = randomGenerator.nextInt(1000);
        waitForClickAbilityOf(emailField);
        driver.findElement(emailField).click();
        driver.findElement(emailField3).sendKeys("imran"+randomInt+"@gmail.com");

        String password = "Login@12345";
        waitForClickAbilityOf(passwordField);
        driver.findElement(passwordField).click();
        driver.findElement(passwordField2).sendKeys(password);

        waitForClickAbilityOf(confirmPasswordField);
        driver.findElement(confirmPasswordField).click();
        driver.findElement(confirmPasswordField2).sendKeys(password);

        waitForClickAbilityOf(signUpBtn);
        driver.findElement(signUpBtn).click();
        waitForVisibilityOf(profileAvatar);
        System.out.println("9. Sign up an account completed");

        waitForClickAbilityOf(newsletterBtn);
        driver.findElement(newsletterBtn).click();
        waitForClickAbilityOf(enableSubscribeToggleBtn);
        driver.findElement(enableSubscribeToggleBtn).click();
        System.out.println("10. Turned on the subscription newsletter from user account.");
        System.out.println("== Tour Booking Test ==");
    }

    @AfterTest
    public void endTest(){
        driver.quit();
    }

    //wait max 30 second until element is visible
    protected void waitForVisibilityOf(By locator) {
        WebDriverWait wait = new WebDriverWait(driver, 30);
        wait.until(ExpectedConditions.visibilityOfElementLocated(locator));
    }

    //wait max 30 second until element is clickable
    protected void waitForClickAbilityOf(By locator) {
        WebDriverWait wait = new WebDriverWait(driver, 30);
        wait.until(ExpectedConditions.elementToBeClickable(locator));
    }
}
