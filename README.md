## Project Information
- Language: Java
- Project Type: Maven
- Framework: TestNG
- Selenium Webdriver Version: 91.0.4472.101 
- Chrome Browser Version: 91.0.4472.124 (Official Build) (64-bit)

## JDK Information
- java 16.0.1 2021-04-20
- Java(TM) SE Runtime Environment (build 16.0.1+9-24)
- Java HotSpot(TM) 64-Bit Server VM (build 16.0.1+9-24, mixed mode, sharing)

## How it's Working Now
[YouTube](https://youtu.be/_ANrullEBSU)
